# FoundryVTT lang il-HE
This module adds the option to select the Hebrew (עברית) language from the FoundryVTT settings menu. Selecting this option will translate various aspects of the program interface.
## Installation
In the Add-On Modules option click on Install Module and place the following link in the field Manifest URL

https://gitlab.com/eranlahav/foundryvtt-lang-il-he/-/raw/master/Core/module.json

If this option does not work download the core-IL.zip file and extract its contents into the /resources/app/public/modules/ folder
Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.

# שפת פואנדרי - עברית
המודול הזה מוסיף את האופציה לבחור בשפה העברית בתפריט ההגדרות של הפאונדרי. בחירה באופציה זו תתרגם אספקטים שונים של ממשק התוכנה
## התקנה
בתפריט התוספים לחץ על התקן מודול והכנס את הלינק למטה לשדה קישור המניפסט

https://gitlab.com/eranlahav/foundryvtt-lang-il-he/-/raw/master/Core/module.json

אם אוםציה זו אינה עובדת, הורד את core-IL.zip  חלץ את הקבצים והעלה אותם אל /resources/app/public/modules/
ברגע שסיימת, הפעל את המודול בהגדרות העולם שאתה מתעד להתשמש בו ואז שנה את הגדרות השפה

# Localized Modules
The following list are modules that support Hebrew language

# list in progress 
